import Zone3 from './zone3.mjs';
import Puretext from './puretext.mjs';
import History from './history.mjs';

export default class Writeous {

  constructor(elementId, mode = {}) {
    this.mode = mode;
    this.initElements(elementId);
    this.initSettings();
    this.ensureMonospace();
    this.initZone3();
    this.initInternals();
    this.attachSelectionListeners();
    this.attachDOMListeners();
    this.attachScrollbarListeners();
    this.autosizeBuffer();
    this.goTo(0);
    this.fillBufferImmediate = this.fillBuffer;
    this.fillBuffer = this.throttle(this.fillBuffer, 10);
    this.fillBuffer();
  }

  // persistence API
  
  getData() {
    return {
      text: this.puretext.text,
      zone3: this.zone3.getData(),
      history: this.history.getData(),
    };
  }

  setData(data) {
    this.puretext.set(data.text || '');
    this.zone3.setData(data.zone3 || []);
    this.history.setData(data.history || {});
    this.fillBuffer();
  };

  // configuration API

  hideScrollbar() {
    this.scrollbar.style.visibility = 'hidden';
  }

  showScrollbar() {
    this.scrollbar.style.visibility = '';
  }

  alwaysShowScrollbar(alwaysShow) {
    this.settings.alwaysShowScrollbar = alwaysShow;
  }

  // manipulation API

  removeSelection() {
    const index = this.selection.start;;
    const size = this.selectionSize();
    this.removeTextAtIndex(index, size);

    this.selection.start = this.index;
    this.selection.end = this.index;

    this.clearSelection();
    this.fillBuffer();
  }

  removeChars(count) {
    this.removeTextAtIndex(this.index, count);
  }

  removeTextAtIndex(indexArg, countArg) {
    if (countArg === 0) return;

    let index = indexArg;
    let count = countArg;
    if (count < 0) {
      index += count;
      count = -count;
    }

    let p = this.puretext.mapAt(index, this.columns).paragraph;
    const text = this.puretext.remove(index, count);
    let pamount = (text.match(/\n/g) || []).length;
    for (let offset = 1; offset <= pamount; offset += 1) {
      this.removeAllPstyles(p + offset);
    }

    const removedZones = this.zone3.formatTagsByIndex(index, index + count);
    this.zone3.remove(index, count); 
    this.setIndex(index);
    this.fillBuffer();
    this.trigger('removetext', { index, text, removedZones });
  }

  paste() {
    // FIXME: cannot implement yet? async API needed?
    throw new Error('programmatic pasting is not supported at this time');
  }


  insertText(text) {
    this.insertTextAtIndex(this.index, text);
  }

  insertTextAtIndex(index, text) {
    const map = this.puretext.mapAt(index, this.columns);
    const pstyles = this.getPstyles(map.paragraph);
    this.puretext.insert(index, text);
    const zoneQueue = this.consumeZoneQueue();
    const zoneUnqueue = this.consumeZoneUnqueue();
    this.zone3.embed(index, text.length, zoneQueue, zoneUnqueue);

    const newlines = (text.match(/\n/g) || []).length;
    for (let i = 1; i <= newlines; i++) {
      [...pstyles].forEach(pstyle => this.addPstyle(pstyle, map.paragraph + i));
    }

    this.setIndex(index + text.length);
    this.fillBuffer();
    this.showCaret();
    this.trigger('inserttext', { index, text, zoneQueue, zoneUnqueue });
  }

  // info API

  textAtRange(start, end) {
    if (start < end) {
      return this.puretext.text.slice(start, end);
    } else {
      return this.puretext.text.slice(end, start);
    }
  }

  lineCount() {
    return this.puretext.toLines(this.columns).length;
  }

  // position API

  moveIndex(count, mode='') {
    switch(mode) {
      case 'line':
        const linemap = this.puretext.toLinemap(this.columns);
        const map = this.puretext.mapAt(this.index, this.columns);
        const newline = Math.max(0, Math.min(linemap.length - 1, map.line + count));
        const newmap = linemap[newline];
        const offset = this.index - map.index;
        this.setIndex(newmap.index + offset);
        break;
      default:
        this.setIndex(this.index + count);
        break;
    }
  }

  setIndex(index) {
    this.index = Math.max(0, Math.min(this.puretext.length(), index));
    this.clearQueues();
    this.moveCaretToIndex();
    this.scrollToCaret();
  }

  // selection API

  selectedText() {
    return this.textAtRange(this.selection.start, this.selection.end);
  }

  copySelection() {
    // TODO: try the async API before falling back to this
    document.execCommand('copy');
  }

  clearSelection() {
    this.zone3.unapplyMatchingAll(['selected'], ['selected']);
  }

  selectionExists() {
    return this.selectionSize() !== 0;
  }

  selectionSize() {
    return this.selection.end - this.selection.start;
  }

  // event API

  on(wevent, listener) {
    wevent = wevent.toLowerCase();
    this.listeners[wevent] = this.listeners[wevent] || [];
    this.listeners[wevent].push(listener);
  }

  trigger(wevent, data) {
    wevent = wevent.toLowerCase();
    data = { type: wevent, data };
    if (this.listeners[wevent] == null) return;

    this.listeners[wevent].forEach((listener) => listener(data));

    if (this.listeners['*'] == null) return;

    this.listeners['*'].forEach((listener) => listener(data));
  }

  // pstyle API

  // TODO: make this addPstyleHere() and make that "a thing"
  addPstyleHere(pstyle) {
    const paragraph = this.puretext.mapAt(this.index, this.columns).paragraph;
    this.addPstyle(pstyle, paragraph);
  }

  removePstyleHere(pstyle) {
    const paragraph = this.puretext.mapAt(this.index, this.columns).paragraph;
    this.removePstyle(pstyle, paragraph);
  }

  togglePstyleHere(pstyle) {
    const paragraph = this.puretext.mapAt(this.index, this.columns).paragraph;
    this.togglePstyle(pstyle, paragraph);
  }

  addPstyle(pstyle, paragraph) {
    this.getPstyles(paragraph).add(pstyle);
    this.fillBuffer();
  }

  removePstyle(pstyle, paragraph) {
    this.getPstyles(paragraph).delete(pstyle);
    this.fillBuffer();
  }

  togglePstyle(pstyle, paragraph) {
    const pstyles = this.getPstyles(paragraph);
    if (pstyles.has(pstyle)) {
      pstyles.delete(pstyle);
    } else {
      pstyles.add(pstyle);
    }

    this.fillBuffer();
  }

  getPstyles(paragraph) {
    this.pstyles[paragraph] = this.pstyles[paragraph] || new Set()
    return this.pstyles[paragraph];
  }

  removeAllPstyles(paragraph) {
    this.pstyles[paragraph] = new Set();
  }


  // zone API

  addZonesToRange(zones, start, end){
    let size = end - start;
    let index = start;
    if (size < 0) {
      size = -size;
      index -= size;
    }

    this.zone3.apply(index, size, zones);
    this.fillBuffer();
  }

  removeZonesFromRange(zones, sl, sc, el, ec) {
    const startIndex = this.toIndex(sl, sc);
    const endIndex = this.toIndex(el, ec);
    let size = endIndex - startIndex;
    let start = startIndex;
    if (size < 0) {
      size = -size;
      start -= size;
    }

    this.zone3.unapply(start, size, zones);
    this.fillBuffer();
  }

  toggleZonesAtRange(zones, sl, sc, el, ec) {
    const startIndex = this.toIndex(sl, sc);
    const endIndex = this.toIndex(el, ec);
    let size = endIndex - startIndex;
    let start = startIndex;
    if (size < 0) {
      size = -size;
      start -= size;
    }

    if (this.zone3.getZone(start + 1).hasAllTags(zones)) {
      this.zone3.unapply(start, size, zones);
    } else {
      this.zone3.apply(start, size, zones);
    }
    this.fillBuffer();
  }

  addZonesToSelection(zones) {
    this.zone3.applyMatchingAll(['selected'], zones);
    this.fillBuffer();
  }

  removeZonesFromSelection(zones) {
    this.zone3.unapplyMatchingAll(['selected'], zones);
    this.fillBuffer();
  }

  toggleZonesAtSelection(zones) {
    // TODO: do this for all zones
    const first = this.zone3.zonesWithAll(['selected'])[0];
    if (first == null) return;

    if (first.hasAllTags(zones)) {
      this.removeZonesFromSelection(zones);
    } else {
      this.addZonesToSelection(zones);
    }
  }

  queueZone(zone) {
    this.zoneQueue.add(zone);
    this.zoneUnqueue.delete(zone);
    this.showCaret();
  }

  dequeueZone(zone) {
    this.zoneQueue.delete(zone);
    this.zoneUnqueue.add(zone);
    this.showCaret();
  }

  toggleZoneQueue(zone) {
    if (!this.zoneUnqueue.has(zone) 
      && (this.zone3.tagsAt(this.index).includes(zone) || this.zoneQueue.has(zone))) {
      this.dequeueZone(zone);
    } else {
      this.queueZone(zone);
    }
  }

  clearQueues() {
    this.zoneQueue = new Set();
    this.zoneUnqueue = new Set();
  }

  // caret API

  showCaret() {
    this.caret.style.display = "block";
    const tags = new Set(this.zone3.tagsAt(this.index));
    this.zoneQueue.forEach(t => tags.add(t));
    this.zoneUnqueue.forEach(t => tags.delete(t));
    const exclude = ['selected'];
    const zones = [...tags].filter(tag => !exclude.includes(tag))
    const className = zones.map(tag => `z-${tag}`).join(' ');
    if (this.caret.className !== 'writeous-caret ' + className) {
      this.caret.className = 'writeous-caret ' + className;
      this.trigger('zonechange', zones);
    }

    // FIXME: hack because caret symbol doesn't show bold style. Find a better way.
    const fontWeight = Number(getComputedStyle(this.caret).fontWeight);
    if (fontWeight > 400) {
      this.caret.textContent = this.caretChars.heavy;
    } else {
      this.caret.textContent = this.caretChars.light;
    }
  }

  hideCaret() {
    this.caret.style.display = "none";
  }

  // internal processing

  undoLast() {
    this.history.undo();
  }

  redoLast() {
    this.history.redo();
  }
  
  updateScrollbar() {
    const scrollbar = this.scrollbar;
    let knob = scrollbar.firstChild;
    let sbheight = scrollbar.clientHeight;
    let sbstyle = getComputedStyle(scrollbar);
    let sbpadding = parseInt(sbstyle.paddingTop.slice(0,-2)) + parseInt(sbstyle.paddingBottom.slice(0,-2));
    let kheight = knob.offsetHeight;

    let ratio = Math.min(1, this.bufferSize / this.lineCount());

    if (ratio === 1 && !this.setting_alwaysShowScrollbar) {
      this.hideScrollbar();
    }
    else {
      this.showScrollbar();
    }

    let newHeight = parseInt((sbheight - sbpadding) * ratio);
    knob.style.height = newHeight + 'px';

    let offsetRatio = this.line / (this.lastLine());
    let newOffset = parseInt((sbheight - kheight - sbpadding) * offsetRatio);
    knob.style.top = newOffset + 'px';
  }

  fillBuffer() {
    let startLine = Math.max(0, this.line - this.overflowSize);
    let endLine = this.line + this.bufferSize + this.overflowSize;

    for(let i = 0; i < this.bufferSize + (this.overflowSize * 2); i++) {

      let lineNumber = i + this.line - this.overflowSize;
      if (lineNumber < 0) continue;

      if (lineNumber >= this.lineCount()) {
        const rowContent  = this.buffer[i].firstChild;
        let child;
        // FIXME: wouldn't it be better to just delete rowContent?
        while(child = rowContent.lastChild) {
          child.remove();
        }
        const emptySpan = document.createElement('span');
        rowContent.appendChild(emptySpan);
        continue;
      }

      const linemap = this.puretext.toLinemap(this.columns);
      const map = linemap[lineNumber];
      const lineData = map.text.replace('\n', '');;

      // FIXME: find a way to poll zone3 once per fillBuffer() rather than once per line.
      const startIndex = map.index;
      const endIndex = startIndex + lineData.length;
      let zoneSegs = this.zone3.formatTagsByIndex(startIndex, endIndex, (tags) => { 
        return tags.map((tag) => `z-${tag}`);
      });

      let rowContent = this.buffer[i].firstChild;
      let child;
      // FIXME: wouldn't it be better to just delete rowContent?
      while(child = rowContent.lastChild) {
        child.remove();
      }

      let zoneSpan = document.createElement('span');
      zoneSpan.appendChild(document.createTextNode(lineData + '\n'));
      rowContent.appendChild(zoneSpan);

      rowContent.className = `row-content ${this.formatPstyles(map.paragraph)}`;

      for(let col in zoneSegs) {
        col = parseInt(col);
        let zones = zoneSegs[col];
        let unsplit = this.childAtColumn(rowContent, col);
        let colOffset = col;
        let previous = unsplit.previousSibling;
        while(previous) { 
          colOffset -= previous.textContent.length;
          previous = previous.previousSibling;
        }
        let [former, latter] = this.splitEl(unsplit, colOffset);
        if(former.textContent === "") former.remove();
        if(latter.textContent === "") latter.remove();
        latter.className = zones.join(' ');

      }

      this.buffer[i].setAttribute('data-line', lineNumber);
    }
    this.moveCaretToIndex();
    this.updateScrollbar();
  }

  autosizeBuffer() {
    let elWidth = this.editor_el.clientWidth;
    let elHeight = this.editor_el.clientHeight;
    let fontSize = parseInt(getComputedStyle(this.editor_el).fontSize.slice(0,-2));

    let rows = parseInt(elHeight / fontSize);

    this.bufferSize = rows;

    let bufferDiff = this.buffer.length - (this.bufferSize + (2 * this.overflowSize));

    let newRows = [];
    for (let i = 0; i < -bufferDiff; i++) {
      let row = document.createElement('div');
      let rowContent = document.createElement('span');
      rowContent.className = 'row-content';
      row.className = 'buffer-row';
      let childCount = this.container_el.children.length;
      if(childCount < this.overflowSize || childCount >= this.bufferSize + this.overflowSize) {
        row.style.userSelect = 'none';
      }
      row.appendChild(rowContent);
      this.container_el.appendChild(row);
      newRows.push(row);
    }
    let removedRows = this.buffer.splice(this.buffer.length - bufferDiff, bufferDiff, ...newRows);
    for (let i = 0; i < removedRows.length; i++) {
      removedRows[i].remove();
    }

    this.topPx = this.editor_el.offsetTop;
    this.botPx = this.topPx + this.editor_el.offsetHeight;
    this.heightPx = this.botPx - this.topPx;
    this.lineHeightPx = this.heightPx / this.bufferSize;
    this.leftMarginPx = this.container_el.offsetLeft;

    this.calculateCharWidth();
    let columns = parseInt(elWidth / (this.charWidth));
    this.columns = columns;

    this.fillBuffer();

  }

  // internal helpers

  // set buffer to display at specified line
  goTo(line, fill = true) {
    line = Math.max(0, Math.min(this.lastLine(), line));
    this.line = line;
    if (fill) this.fillBuffer();
  }

  // TODO: move helpers without writeous logic to separate module
  splitEl(el, c) {
    let [first, rest] = [el.textContent.slice(0, c), el.textContent.slice(c)];
    el.textContent = first;
    let newEl = document.createElement(el.tagName);
    newEl.appendChild(document.createTextNode(rest));
    el.parentElement.insertBefore(newEl, el.nextSibling);
    return [el, newEl];
  }

  childAtColumn(el, c) {
    let current = 0;
    const found = [...el.children].find((child) => {
      current += child.textContent.length;
      if (current >= c) return child;
    });
    return found || el.lastChild;
  }

  scrollToCaret() {
    let { line } = this.toPos(this.index);
    if(line < this.line) {
      this.scrollToLine(line);
    }
    else if(line >= (this.line + this.bufferSize - this.focusRowOffset - 1)) {
      this.scrollToLine(line - this.bufferSize + 1);
    }
  }

  moveCaretToIndex() {
    let { x, y } = this.indexToPx(this.index);
    this.caret.style.left = (x + 'px');
    this.caret.style.top = (y + 'px');

    if (document.activeElement != this.el || y < this.topPx || y > this.botPx) {
      this.hideCaret();
    }
    else {
      this.showCaret();
    }

  }

  formatPstyles(paragraph) {
    return [...this.getPstyles(paragraph)].map(pstyle => `p-${pstyle}`).join(' ');
  }

  consumeZoneQueue() {
    let temp = [...this.zoneQueue];
    this.zoneQueue = new Set();
    return temp;
  };

  consumeZoneUnqueue() {
    let temp = [...this.zoneUnqueue];
    this.zoneUnqueue = new Set();
    return temp;
  };

  toIndex(line, column) {
    const linemap = this.puretext.toLinemap(this.columns);
    line = Math.max(0, Math.min(linemap.length - 1, line));
    const map = linemap[line];
    column = Math.max(0, Math.min(map.length, column));
    return map.index + column;
  }

  toPos(index) {
    if (index < 0) return { line: 0, column: 0 };
    const map = this.puretext.mapAt(index, this.columns);
    return { line: map.line, column: Math.max(0, index - map.index) };
  }

  pxToIndex(x, y) {
    const { line, column } = this.pxToPos(x, y);
    return this.toIndex(line, column);
  }

  indexToPx(index) {
    const { line, column } = this.toPos(index);
    return this.posToPx(line, column);
  }

  pxToPos(x, y) {
    let line;
    const lineOffset = (y - this.topPx) / this.lineHeightPx;
    line = Math.floor(this.line + lineOffset);
    line = Math.max(this.line, Math.min(this.line + this.bufferSize - 1, line));
    line = Math.max(0, Math.min(this.lineCount() - 1, line));
    let row = this.buffer[this.overflowSize + (line - this.line)].firstChild;
    let column = (x - (this.leftMarginPx + row.offsetLeft)) / this.charWidth;
    column = Math.round(column);
    const columns = row.textContent.length - 1;
    column = Math.max(0, Math.min(columns, column));

    return { line, column };
  }

  posToPx(line, column) {
    let x = 0;
    let y = 0;
    const row = this.buffer[this.overflowSize + (line - this.line)];
    if (row != null) {
      x = this.leftMarginPx + row.firstChild.offsetLeft + (this.charWidth * column);
    }

    const lineOffset = line - this.line;
    y = this.topPx + (this.lineHeightPx * lineOffset);
    return { x, y };
  }

  calculateCharWidth() {
    let firstRow = this.buffer[this.overflowSize];
    let rowContent = firstRow.firstChild;

    let child;
    while(child = rowContent.lastChild) {
      child.remove();
    }

    let span = document.createElement('span');
    span.appendChild(document.createTextNode(' '.repeat(this.columns)));
    rowContent.appendChild(span);
    this.charWidth = span.offsetWidth / this.columns;

    //this.fontSize = parseFloat(getComputedStyle(this.el).fontSize.slice(0, -2));
    //this.charWidth = 0.6 * this.fontSize;
  }

  // TODO: move this into a helper library with debounce() from Aventure
  throttle(func, limit) {
    let lastFunc
      let lastRan
      return function() {
        const context = this
          const args = arguments
          if (!lastRan) {
            func.apply(context, args)
              lastRan = Date.now()
          } else {
            clearTimeout(lastFunc)
              lastFunc = setTimeout(function() {
                if ((Date.now() - lastRan) >= limit) {
                  func.apply(context, args)
                    lastRan = Date.now()
                }
              }, limit - (Date.now() - lastRan))
          }
      }
  }

  // The last line for this.line
  lastLine() {
    return this.lineCount() - this.bufferSize;
  }

  // initialization

  attachScrollbarListeners() {
    this.attachScrollListener();
    let scrollKnobDragUpdate;
    document.addEventListener('mouseup', (event) => {
      clearInterval(scrollKnobDragUpdate);
    });

    this.scrollbar.addEventListener('mousedown', (event) => {
      scrollKnobDragUpdate = setInterval( () => {
        let offsetY = this.mousey - this.scrollbar.offsetTop;
        let newLine = parseInt((offsetY / this.scrollbar.clientHeight) * (this.lastLine()));
        this.goTo(newLine);
        this.updateScrollbar();
      }, 20);
    });
  }

  attachSelectionListeners() {
    let selectionDragUpdate = null;
    document.addEventListener('mouseup', (event) => {
      clearInterval(selectionDragUpdate);
      selectionDragUpdate = null;
    });

    this.el.addEventListener('mousedown', (event) => {
      const index = this.pxToIndex(this.mousex, this.mousey);
      this.setIndex(index);
      this.moveCaretToIndex();

      if(!selectionDragUpdate) {
        this.selection = {start: 0, end: 0};
        this.selection.start = this.index;
        this.selection.end = this.index;
        selectionDragUpdate = setInterval( () => {
          const index = this.pxToIndex(this.mousex, this.mousey);
          this.setIndex(index);
          this.selection.end = this.index;
          this.zone3.unapplyMatchingAll(['selected'], ['selected']);
          this.clearSelection();
          this.addZonesToRange(['selected'], this.selection.start, this.selection.end);
          if(this.mousey > this.botPx) {
            this.scrollAmount = 10;
            this.scrollDirection = 10;
          }
          else if(this.mousey < this.topPx) {
            this.scrollAmount = 10;
            this.scrollDirection = -10;
          }
        }, 20);
      }
    });
  }

  attachDOMListeners() {
    window.addEventListener('resize', (event) => {
      this.autosizeBuffer();
    });

    this.el.addEventListener('keydown', this.handle_keydown.bind(this));
    this.el.addEventListener('keypress', this.handle_keypress.bind(this));
    this.el.addEventListener('focusout', this.handle_focusout.bind(this));
    document.addEventListener('paste', this.handle_paste.bind(this));
    document.addEventListener('copy', this.handle_copy.bind(this));

    document.addEventListener('mousemove', (event) => {
      this.mousex = event.clientX;
      this.mousey = event.clientY;
    });
  }

  initInternals() {
    this.index = 0;
    this.line = 0;
    this.buffer = [];
    this.puretext = new Puretext();
    this.pstyles = {};
    this.listeners = {};
    this.history = new History(this);

    // for scroll animation
    this.scrollAmount = 0;
    this.scrollDirection = 0;

    this.scrollData = {
      scrollEvent: null,
      scrollforth: null,
      scrollback: null,
      scrollInterval: null,
    }

    // layout data
    this.fontSize = 0;
    this.charWidth = 0;
    this.topPx = 0;
    this.botPx = 0;
    this.heightPx = 0;
    this.leftMarginPx = 0;


    // TODO: put these in settings?
    this.bufferSize = 30;
    this.overflowSize = 5;
    this.columns = 90;
    this.focusRowOffset = 0;

  }

  initZone3() {
    this.zone3 = new Zone3(0, []);
    this.zoneQueue = new Set();
    this.zoneUnqueue = new Set();
  }

  initElements(elementId) {
    // main el
    this.el = document.getElementById(elementId);
    if (this.el == null) throw new Error(`writeous could not find an element with id "${elementId}"`);

    this.el.setAttribute('tabindex', -1);
    this.el.className = 'writeous';

    // scrollbar
    this.scrollbar = document.createElement('div');
    this.scrollbar.className = 'scrollbar';
    const knob = document.createElement('div');
    knob.className = 'knob';
    this.scrollbar.appendChild(knob);
    this.el.appendChild(this.scrollbar);

    // editor area
    this.editor_el = document.createElement('div');
    this.editor_el = document.createElement('div');
    this.editor_el.className = 'writeous-editor';
    this.el.appendChild(this.editor_el);

    // text container
    this.container_el = document.createElement('div');
    this.container_el.className = 'writeous-container';
    this.editor_el.appendChild(this.container_el);

    // caret
    this.caret = document.createElement('span');
    this.caretChars = {
      light: '▏',
      heavy: '▎',
    }
    this.caret.appendChild(document.createTextNode(this.caretChars.light));
    this.caret.className = 'writeous-caret';
    this.el.appendChild(this.caret);
  }

  initSettings() {
    this.settings = {
      alwaysShowScrollbar: false,
      scrollSensitivity: 4,
    };
  }

  ensureMonospace() {
    const testSpan = document.createElement('span');
    testSpan.style.visibility = 'hidden';
    this.container_el.appendChild(testSpan);
    testSpan.textContent = 'i';
    const i_width = testSpan.offsetWidth;
    testSpan.textContent = 'X';
    const X_width = testSpan.offsetWidth;
    testSpan.remove();

    if (!(i_width === X_width)) {
      console.warn('detected a non-monospace font. falling back to monospace. (writeous currently only supports monospace fonts)');
      this.el.style.fontFamily = 'monospace';
    }
  }

  // internal scroll logic

  attachScrollListener() {
    this.scrollData.scrollInterval = setInterval(() => {
      if (this.scrollAmount > 0) {
        this.handle_scroll();
      }
    }, 20);

    this.el.addEventListener('wheel', this.incrementScroll.bind(this));
  }

  incrementScroll(event) {
    // only negative when deltaY and this.scrollDirection are opposite directions
    if(event.deltaY * this.scrollDirection < 0) {
      this.scrollAmount = 1;
    }
    else if(this.scrollAmount === 0) {
      this.scrollAmount = 1;
    }
    else {
      this.scrollAmount += this.settings.scrollSensitivity;
    }
    this.scrollDirection = event.deltaY;
    this.scrollData.scrollEvent = event;
  }

  // TODO: this could be in a public API
  scrollToLine(line) {
    const diff = line - this.line;
    this.scrollAmount = Math.abs(diff);
    this.scrollDirection = diff;
  }

  new_handle_scroll() {
    const lineAdjust = (this.scrollDirection > 0) ? 1 : -1;
    this.goTo(this.line + lineAdjust);
    this.scrollAmount -= 1;
  }

  handle_scroll() {
    let lineAdjust = 1;
    if(this.scrollDirection > 0) {
      if(this.line >=  this.lastLine()) return;
      if(this.scrollData.scrollforth) return; 

      if(this.scrollData.scrollback) {
        this.scrollData.scrollback.cancel();
        this.scrollData.scrollback = null;
      }

      //TODO: give caret a proper scroll animation
      this.hideCaret();
      console.log('creating scrollforth');
      this.scrollData.scrollforth = this.container_el.animate(
          [
          {
            top: (-this.overflowSize) + 'em',
          },
          {
            top: (-this.overflowSize - 1) + 'em',
          }
          ],
          {
            id: 'scrollforth',
            duration: 100,
            easing: 'linear',
            fill: 'forwards',
            iterations: 1
          }
          );
      this.scrollData.scrollforth.onfinish = function() {
        this.scrollAmount --;
        this.goTo(this.line + lineAdjust, false);
        this.fillBufferImmediate();
        //this.setSelectionToPos();
        if(this.scrollAmount > 0 && this.line < this.lastLine()) {
          if(this.scrollData.scrollback) {
            console.log('scrollback canceled!');
            this.scrollData.scrollback.cancel();
            this.scrollData.scrollback = null;
          }
          console.log('scrollback replayed!');
          this.scrollData.scrollforth.playbackRate = this.scrollAmount;
          this.scrollData.scrollforth.play();
        }
        else {
          // probably don't need to delete this anymore
          this.scrollData.scrollforth.cancel();
          this.scrollData.scrollforth = null;
          this.scrollAmount = 0;

          this.moveCaretToIndex();
        }
      }.bind(this);
    }
    else {
      if(this.line <= 0) return;
      if(this.scrollData.scrollback) return;

      if(this.scrollData.scrollforth) {
        this.scrollData.scrollforth.cancel();
        this.scrollData.scrollforth = null;
      }

      this.hideCaret();
      console.log('creating scrollback');
      this.scrollData.scrollback = this.container_el.animate(
          [
          {
            top: (-this.overflowSize) + 'em',
          },
          {
            top: (-this.overflowSize + lineAdjust) + 'em',
          }
          ],
          {
            id: 'scrollback',
            duration: 100,
            easing: 'linear',
            fill: 'forwards',
            iterations: 1
          }
          );
      this.scrollData.scrollback.onfinish = function() {
        this.scrollAmount --;
        console.log(this.scrollAmount);
        this.goTo(this.line - lineAdjust, false);
        this.fillBufferImmediate();
        if(this.scrollAmount > 0 && this.line > 0) {
          if(this.scrollData.scrollforth) {
            this.scrollData.scrollforth.cancel();
            this.scrollData.scrollforth = null;
          }
          this.scrollData.scrollback.playbackRate = this.scrollAmount;
          this.scrollData.scrollback.play();
        }
        else {
          this.scrollData.scrollback.cancel();
          this.scrollData.scrollback = null;
          this.scrollAmount = 0;
          this.moveCaretToIndex();
        }
      }.bind(this);

    }
  }

  // DOM event handlers

  handle_keydown(event) {
    const callback_keys = [];

    if (event.ctrlKey) callback_keys.push('Control');

    if (event.altKey) callback_keys.push('Alt');

    // some characters aren't affected by Shift when the numpad is used
    // TODO: should be in a helper somewhere
    const unaffectedByShift = event.key.length > 1 || ['/', '*', '-', '+'].includes(event.key);
    if (event.shiftKey && unaffectedByShift) callback_keys.push('Shift');

    // prevent Control, Alt or Shift from being added twice.
    if (!callback_keys.includes(event.key)) callback_keys.push(event.key);

    const callback_string = callback_keys.join(' ');
    const callback = this.mode[callback_string];
    if (callback != null) {
      let text = callback.bind(this)(event);
      if (text) {
        const insertCb = this.mode['Insert'];
        text = (insertCb != null) ? insertCb.bind(this)(event, text) : text;
        if (text) this.insertText(text);
      }
    }
  }

  handle_keypress(event) {
    if (!this.mode[event.key]) {
      const character = String.fromCharCode(event.charCode);
      if (event.charCode > 0 && character) {
        const callback = this.mode['Insert'];
        const c = (callback != null) ? callback.bind(this)(event, event.key) : event.key;
        if (c) this.insertText(c);
      }
    }
  }

  handle_paste(event) {
    const pastedText = event.clipboardData.getData('text/plain');
    const callback = this.mode['PasteText'];
    if (callback != null) {
      const text = callback.bind(this)(event, pastedText);
      if (text) {
        this.insertText(text);
      }
    }
  }

  handle_copy(event) {
    event.clipboardData.setData('text/plain', this.selectedText());
    event.preventDefault();
    event.stopPropagation();
  }

  handle_focusout(event) {
    this.hideCaret();
  }

}
