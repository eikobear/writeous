import test from "tape";
import Zone3 from "../zone3.mjs";

// apply zone - add new label to range
// DONE: insert zone - add new zone with new label inside existing zone
// embed zone - add new zone with new label inside existing zone, keeping old tags
// DONE: replace zone - replace tags in range with new label
// (zone size)--(zone size)--(zone size)


test('zone creation', (t) => {
  const z3 = new Zone3(54, ['bold', 'italic']);
  t.deepEqual(z3.head.getTags(), ['bold', 'italic'], 'default tags are applied to first zone');
  t.deepEqual(z3.head.size, 54, 'default size is applied to first zone');
  t.end();
});

test('appending zones', (t) => {
  const z3 = new Zone3(3, ['one']);
  z3.head.append(1, ['two']);
  z3.head.n.append(9, ['three']);
  t.deepEqual(z3.head.getTags(), ['one'], 'zone one is first');
  t.deepEqual(z3.head.n.getTags(), ['two'], 'zone two is second');
  t.deepEqual(z3.head.n.n.getTags(), ['three'], 'zone three is third');
  t.end();
});

test('insertAfter', (t) => {
  const z3 = new Zone3(3, ['one']);
  z3.append(1, ['three']);
  z3.head.insertAfter(9, ['two']);
  t.deepEqual(z3.head.getTags(), ['one'], 'zone one is first');
  t.deepEqual(z3.head.n.getTags(), ['two'], 'zone two is second');
  t.deepEqual(z3.head.n.n.getTags(), ['three'], 'zone three is third');
  t.end();
});

test('zone at index', (t) => {
  const z3 = new Zone3(1, ['one']);
  z3.append(3, ['two']);
  z3.append(2, ['three']);
  z3.append(4, ['four']);
  t.deepEqual(z3.tagsAt(0), ['one'], 'zone one is at index 0');
  t.deepEqual(z3.tagsAt(1), ['one'], 'zone one is at index 1');
  t.deepEqual(z3.tagsAt(2), ['two'], 'zone two is at index 2');
  t.deepEqual(z3.tagsAt(4), ['two'], 'zone two is at index 4');
  t.deepEqual(z3.tagsAt(5), ['three'], 'zone three is at index 5');
  t.deepEqual(z3.tagsAt(6), ['three'], 'zone three is at index 6');
  t.deepEqual(z3.tagsAt(7), ['four'], 'zone four is at index 7');
  t.deepEqual(z3.tagsAt(10), ['four'], 'zone four is at index 10');
  t.throws( () => { z3.zoneAt(-1) }, 'index -1 is out of bounds');
  t.equal(z3.zoneAt(11), null, 'zone at index 11 is null');
  t.end();
});

test('getIndex', (t) => {
  const z3 = new Zone3(2, ['one']);
  z3.append(5, ['two']);
  z3.append(3, ['three']);
  t.equal(z3.head.getIndex(), 0, 'one is at index 0');
  t.equal(z3.head.n.getIndex(), 2, 'two is at index 2');
  t.equal(z3.head.n.n.getIndex(), 7, 'three is at index 7');
  t.end();
});

test('replace zone', (t) => {
  const z3 = new Zone3(3, ['one']);
  z3.append(2, ['two']);
  z3.append(5, ['three']);
  t.deepEqual(z3.tagsAt(0), ['one'], 'zone one is at index 0');
  t.deepEqual(z3.tagsAt(3), ['one'], 'zone one is at index 3');
  t.deepEqual(z3.tagsAt(4), ['two'], 'zone two is at index 4');
  t.deepEqual(z3.tagsAt(5), ['two'], 'zone two is at index 5');
  t.deepEqual(z3.tagsAt(6), ['three'], 'zone three is at index 6');
  t.deepEqual(z3.tagsAt(10), ['three'], 'zone three is at index 10');
  t.throws( () => { z3.zoneAt(-1) }, 'index -1 is out of bounds');
  t.equal(z3.zoneAt(11), null, 'zone at index 11 is null');
  z3.replace(1, 5, ['replace']);
  t.deepEqual(z3.tagsAt(0), ['one'], 'zone one is at index 0');
  t.deepEqual(z3.tagsAt(2), ['replace'], 'zone replace is at index 2');
  t.deepEqual(z3.tagsAt(6), ['replace'], 'zone replace is at index 6');
  t.deepEqual(z3.tagsAt(7), ['three'], 'zone three is at index 7');
  t.deepEqual(z3.tagsAt(10), ['three'], 'zone three is at index 10');
  t.throws( () => { z3.zoneAt(-1) }, 'index -1 is out of bounds');
  t.equal(z3.zoneAt(11), null, 'zone at index 11 is null');
  t.end();
});

test('insert zone', (t) => {
  const z3 = new Zone3(5, ['one']);
  z3.insert(3, 4, ['insert']);
  t.deepEqual(z3.tagsAt(0), ['one'], 'zone one at index 0');
  t.deepEqual(z3.tagsAt(3), ['one'], 'zone one at index 3');
  t.deepEqual(z3.tagsAt(4), ['insert'], 'zone insert at index 4');
  t.deepEqual(z3.tagsAt(7), ['insert'], 'zone insert at index 7');
  t.deepEqual(z3.tagsAt(8), ['one'], 'zone one at index 8');
  t.deepEqual(z3.tagsAt(9), ['one'], 'zone one at index 9');
  t.equal(z3.zoneAt(10), null, 'zone at index 10 is null');
  t.end();
});

test('embed zone', (t) => {
  const z3 = new Zone3(5, ['A']);
  z3.embed(3, 4, ['B']);
  z3.embed(4, 3, ['C']);
  z3.embed(5, 1, ['D'], ['B']);
  t.deepEqual(z3.tagsAt(0), ['A'], 'zone A at index 0');
  t.deepEqual(z3.tagsAt(4), ['A', 'B'], 'zone AB at index 4');
  t.deepEqual(z3.tagsAt(5), ['A', 'B', 'C'], 'zone ABC at index 5');
  t.deepEqual(z3.tagsAt(6), ['A', 'C', 'D'], 'zone B is excepted at index 6'); 
  t.deepEqual(z3.tagsAt(9), ['A', 'B'], 'zone AB at index 8');
  t.deepEqual(z3.tagsAt(12), ['A'], 'zone A at index 11');
  t.deepEqual(z3.tagsAt(13), ['A'], 'zone A at index 12');
  t.equal(z3.zoneAt(14), null, 'zone at index 13 is null');
  t.end();
});

test('apply zone', (t) => {
  const z3 = new Zone3(3, ['A']);
  z3.append(1, ['B']);
  z3.append(2, ['C']);
  z3.append(4, ['D']);
  z3.apply(1, 7, ['X']);
  t.deepEqual(z3.tagsAt(0), ['A'], 'zone A at 0');
  t.deepEqual(z3.tagsAt(1), ['A'], 'zone A at 1');
  t.deepEqual(z3.tagsAt(2), ['A', 'X'], 'zone AX at 2');
  t.deepEqual(z3.tagsAt(3), ['A', 'X'], 'zone AX at 3');
  t.deepEqual(z3.tagsAt(4), ['B', 'X'], 'zone BX at 4');
  t.deepEqual(z3.tagsAt(5), ['C', 'X'], 'zone CX at 5');
  t.deepEqual(z3.tagsAt(6), ['C', 'X'], 'zone CX at 6');
  t.deepEqual(z3.tagsAt(7), ['D', 'X'], 'zone DX at 7');
  t.deepEqual(z3.tagsAt(8), ['D', 'X'], 'zone DX at 8');
  t.deepEqual(z3.tagsAt(9), ['D'], 'zone D at 9');
  t.deepEqual(z3.tagsAt(10), ['D'], 'zone D at 10');
  t.equal(z3.zoneAt(11), null, 'zone at index 11 is null');
  t.end();
});

test('apply inside single zone', (t) => {
  const z3 = new Zone3(10, ['A']);
  z3.apply(3, 5, 'B');
  t.deepEqual(z3.tagsAt(0), ['A'], 'zone A at 0');
  t.deepEqual(z3.tagsAt(3), ['A'], 'zone A at 3');
  t.deepEqual(z3.tagsAt(4), ['A', 'B'], 'zone AB at 4');
  t.deepEqual(z3.tagsAt(8), ['A', 'B'], 'zone AB at 8');
  t.deepEqual(z3.tagsAt(9), ['A'], 'zone A at 9');
  t.deepEqual(z3.tagsAt(10), ['A'], 'zone A at 10');
  t.equal(z3.zoneAt(11), null, 'null at index 11');
  t.end();
});

test('duplicate tags', (t) => {
  const z3 = new Zone3(1, ['A', 'B', 'A']);
  t.deepEqual(z3.tagsAt(0), ['A', 'B'], 'duplicate tags are removed from new Zone3 head');
  z3.append(1, ['C', 'C']);
  t.deepEqual(z3.tagsAt(2), ['C'], 'duplicate tags are removed when appending');
  z3.apply(0, 1, ['X', 'X']);
  t.deepEqual(z3.tagsAt(0), ['A', 'B', 'X'], 'duplicate tags are removed when applying');
  z3.insert(1, 1, ['Z', 'D', 'D']);
  t.deepEqual(z3.tagsAt(2), ['Z', 'D'], 'duplicate tags are removed when inserting');
  z3.replace(0, 5, ['W', 'W', 'W']);
  t.deepEqual(z3.tagsAt(1), ['W'], 'duplicate tags are removed when replacing');
  z3.embed(2, 1, ['1', '2', '1', '2']);
  t.deepEqual(z3.tagsAt(3), ['W', '1', '2'], 'duplicate tags are removed when embedding');
  t.end();

});

// TODO: rewrite test for formatTagsByIndex
test.skip('formatTagsByLine(startLine, endLine, columnWidth, formatter)', (t) => {
  
  const z3 = new Zone3(113);
  z3.append(14, ['bold']);
  z3.append(140, []);
  z3.append(75, ['bold', 'italic']);
  z3.append(14, ['bold']);
  z3.append(100, []);

  let formatter = (tags) => {
    return tags.map((tag) => `z-${tag}`);
  };
  const tags = z3.formatTagsByLine(1, 10, 50, formatter);
  t.deepEqual(tags[0], { 0: [] }, 'line 1 is empty');
  t.deepEqual(tags[1], { 0: [], 13: ['z-bold'], 27: [] }, 'line 2 has a bold segment');
  t.deepEqual(tags[2], { 0: [] }, 'line 3 is empty');
  t.deepEqual(tags[3], { 0: [] }, 'line 4 is empty');
  t.deepEqual(tags[4], { 0: [], 17: ['z-bold', 'z-italic'] }, 'line 5 starts bold and italic');
  t.deepEqual(tags[5], { 0: ['z-bold', 'z-italic'], 42: ['z-bold'] }, 'line 6 closes bold but stays italic');
  t.deepEqual(tags[6], { 0: ['z-bold'], 6: [] }, 'line 7 closes bold');
  t.deepEqual(tags[7], { 0: [] }, 'line 8 is empty');
  t.deepEqual(tags[8], { 0: [] }, 'line 9 is empty');
  t.equal(typeof(tags[9]), 'undefined', 'line 10 is undefined');
  t.end();
});

test('remove(index, size)', (t) => {
  const z3 = new Zone3(3, ['A']);
  z3.append(5, ['B']);
  t.deepEqual(z3.tagsAt(0), ['A'], 'regression');
  t.deepEqual(z3.tagsAt(3), ['A'], 'regression');
  t.deepEqual(z3.tagsAt(4), ['B'], 'regression');
  t.deepEqual(z3.tagsAt(8), ['B'], 'regression');

  z3.remove(1, 4);

  t.deepEqual(z3.tagsAt(0), ['A'], 'after remove');
  t.deepEqual(z3.tagsAt(1), ['A'], 'after remove');
  t.deepEqual(z3.tagsAt(2), ['B'], 'after remove');
  t.deepEqual(z3.tagsAt(4), ['B'], 'after remove');
  t.deepEqual(z3.zoneAt(5), null, 'after remove');
  t.end();
});

