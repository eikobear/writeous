import test from 'tape';
import Puretext from '../puretext.mjs';

test('Puretext construction', (t) => {
  const text = 'test text';
  const pt = new Puretext(text);
  t.equal(pt.text, text);
  t.end();
});

test('inserting text', (t) => {
  const text = 'test text';
  const inserted = 'test hellotext';
  const pt = new Puretext(text);
  pt.insert(5, 'hello');
  t.equal(pt.text, inserted);
  t.end();
});

test('removing text', (t) => {
  const text = 'test text';
  const removed = 'text';
  const pt = new Puretext(text);
  pt.remove(2, 5);
  t.equal(pt.text, removed);
  t.end();
});

test('to lines', (t) => {
  const text = 'test\nnospacetest\nword test';
  const c = 6;
  const lines = ['test\n', 'nospac', 'etest\n', 'word ', 'test'];
  const pt = new Puretext(text);
  t.deepEqual(pt.toLines(c), lines);
  t.end();
});
