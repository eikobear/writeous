
const map = {};

map['Backspace'] = function(event) {
  event.preventDefault();
  event.stopPropagation();
  if (this.selectionExists()) {
    this.removeSelection();
  } else {
    this.removeChars(-1);
  }
};

map['Tab'] = function(event) {
  event.preventDefault();
  event.stopPropagation();
  return '    ';
};

map['Delete'] = function(event) {
  event.preventDefault();
  event.stopPropagation();
  if (this.selectionExists()) {
    this.removeSelection();
  } else {
    this.removeChars(1);
  }
};

map['Control b'] = function(event) {
  event.preventDefault();
  event.stopPropagation();

  if (this.selectionExists()) {
    this.toggleZonesAtSelection(['bold']);
  } else {
    this.toggleZoneQueue('bold');
  }
}

map['Control |'] = function(event) {
  event.preventDefault();
  event.stopPropagation();
  this.addPstyleHere('center');
  this.removePstyleHere('right');
}

map['Control }'] = function(event) {
  event.preventDefault();
  event.stopPropagation();
  this.addPstyleHere('right');
  this.removePstyleHere('center');
}

map['Control {'] = function(event) {
  event.preventDefault();
  event.stopPropagation();
  this.removePstyleHere('right');
  this.removePstyleHere('center');
}

map['Control i'] = function(event) {
  event.preventDefault();
  event.stopPropagation();

  if (this.selectionExists()) {
    this.toggleZonesAtSelection(['italic']);
  } else {
    this.toggleZoneQueue('italic');
  }
}

map['Control c'] = function(event) {
  event.preventDefault();
  event.stopPropagation();
  this.copySelection();
}

map['Control z'] = function(event) {
  event.preventDefault();
  event.stopPropagation();
  this.undoLast();
}

map['Control r'] = function(event) {
  event.preventDefault();
  event.stopPropagation();
  this.redoLast();
}

map['ArrowLeft'] = function(event) {
  this.moveIndex(-1);
};

map['ArrowRight'] = function(event) {
  this.moveIndex(1);
};

map['ArrowUp'] = function(event) {
  this.moveIndex(-1, 'line');
};

map['ArrowDown'] = function(event) {
  this.moveIndex(1, 'line');
};

map['Enter'] = function(event) {
  return '\n';
};

map['PasteText'] = function(event, text) {
  this.removeSelection();
  return text;
}

map['Insert'] = function(event, character) {
  this.removeSelection();
  return character;
}

export default map;

