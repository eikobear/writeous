
export default class Puretext {

  constructor(text = '') {
    this.text = text;
    this.cache = {};
  }

  set(text) {
    this.text = text;
    this.cache = {};
  }

  insert(index, text) {
    if (index < 0 || index > this.length()) throw `index ${index} out of bounds`;

    // invalidate cache
    this.cache = {};
    
    const before = this.text.slice(0, index);
    const after = this.text.slice(index);
    this.text = before + text + after;
    return text;
  }

  remove(index, count) {
    if (index < 0 || index > this.length()) throw `index ${index} out of bounds`;
    
    // invalidate cache
    this.cache = {};

    const before = this.text.slice(0, index);
    const after = this.text.slice(index + count);
    const removed = this.text.slice(index, index + count);
    this.text = before + after;
    return removed;
  }

  toLines(c) {
    if (this.cache.lines && this.cache.lines.c === c) return this.cache.lines.data;

    const r = new RegExp(`(.{1,${c - 1}}$)|(.{0,${c - 1}} )|([^ ]{1,${c}})|(^$)`, 'g');
    let lines = [];
   
    // for loop used for speed optimization
    let p = this.text.split('\n');
    for (let i = 0; i < p.length; i++) {
      // push used for in-memory array expansion
      lines.push(...p[i].match(r));
      lines[lines.length - 1] += '\n';
    }

    this.cache.lines = {};
    this.cache.lines.data = lines;
    this.cache.lines.c = c;
    return this.cache.lines.data;
  }

  toLinemap(c) {
    if (this.cache.linemap && this.cache.linemap.c === c) return this.cache.linemap.data;

    const lines = this.toLines(c);
    const map = [];
    let index = 0;
    let line = 0;
    let paragraph = 0;
    lines.forEach((text) => {
      map.push({ index, line, paragraph, text, length: text.length });
      index += text.length;
      line += 1;
      if (text.includes('\n')) paragraph += 1;
    });

    this.cache.linemap = {};
    this.cache.linemap.data = map;
    this.cache.linemap.c = c;
    return this.cache.linemap.data;
  }

  length() {
    if (this.cache.length) return this.cache.length.data;

    this.cache.length = {};
    this.cache.length.data = this.text.length;
    return this.cache.length.data;
  }

  // handle out-of-bounds indices by return first or last map
  mapAt(index, c) {
    const linemap = this.toLinemap(c);
    if (index < 0) return linemap[0];

    for (let i = 0; i < linemap.length; i++) {
      if (linemap[i].index > index) return linemap[i - 1];
    }

    return linemap.slice(-1)[0];
  }

}
