export default class RemoveTextRecord {

  constructor(data) {
    this.index = data.index;
    this.text = data.text;
    this.removedZones = data.removedZones;
  }

  static type() {
    return 'removetext';
  }

  getData() {
    return { index: this.index, text: this.text, removedZones: this.removedZones };
  }

  canMerge(record) {
    if (record.constructor.type() !== RemoveTextRecord.type()) return false;

    if (record.text.includes(' ')) return false;
    
    if (record.text.includes('\n')) return false;

    if (this.index !== record.index + record.text.length) return false;

    return true;
  }

  merge(record) {
    this.index = record.index;
    this.text = record.text + this.text;
  }

  undo(writeous) {
    writeous.insertTextAtIndex(this.index, this.text);
    const keys = Object.keys(this.removedZones);
    for (let i = 0; i < keys.length; i += 1) {
      let offset = Number(keys[i]);
      let next = Number(keys[i + 1]) || this.text.length;
      writeous.zone3.apply(this.index + offset, next - offset, this.removedZones[offset]);
    }
  }

  redo(writeous) {
    writeous.removeTextAtIndex(this.index, this.text.length);
  }
}
