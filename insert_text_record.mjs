export default class InsertTextRecord {

  constructor(data) {
    this.index = data.index;
    this.text = data.text;
    this.zoneQueue = data.zoneQueue;
    this.zoneUnqueue = data.zoneUnqueue;
  }

  getData() {
    return { index: this.index, text: this.text, zoneQueue: this.zoneQueue, zoneUnqueue: this.zoneUnqueue };
  }

  static type() {
    return 'inserttext';
  }

  canMerge(record) {
    if (record.constructor.type() !== InsertTextRecord.type()) return false;

    if (this.text.endsWith(' ')) return false;

    if (this.text.endsWith('\n')) return false;

    if (record.text.includes('\n')) return false;

    if (record.index !== this.index + this.text.length) return false;

    if (record.zoneQueue.length > 0 || record.zoneUnqueue.length > 0) return false;

    return true;
  }

  merge(record) {
    this.text += record.text;
  }

  undo(writeous) {
    writeous.removeTextAtIndex(this.index, this.text.length);
  }

  redo(writeous) {
    this.zoneQueue.forEach(zone => writeous.queueZone(zone));
    this.zoneUnqueue.forEach(zone => writeous.dequeueZone(zone));
    writeous.insertTextAtIndex(this.index, this.text);
  }
}
