import InsertTextRecord from './insert_text_record.mjs';
import RemoveTextRecord from './remove_text_record.mjs';

export default class History {
  
  constructor(writeous) {
    this.writeous = writeous;
    this.handlers = {};
    this.addHandler(InsertTextRecord);
    this.addHandler(RemoveTextRecord);
    this.records = [];
    this.redos = [];
    this.recording = true;
    this.overwriteRedos = true;
  }

  getData() {
    let data = { records: [], redos: [] };

    this.records.forEach((record) => {
      data.records.push({ 
        type: record.constructor.type(),
        data: record.getData(),
      });
    });

    this.redos.forEach((redo) => {
      data.redos.push({ 
        type: redo.constructor.type(),
        data: redo.getData(),
      });
    });

    return data;
  }

  setData(data) {
    this.records = [];
    this.redos = [];
    let records = data.records || [];
    records.forEach((wevent) => {
      this.records.push(this.weventToRecord(wevent));
    });

    let redos = data.redos || [];
    redos.forEach((wevent) => {
      this.redos.push(this.weventToRecord(wevent));
    });
  }

  weventToRecord(wevent) {
    const Record = this.handlers[wevent.type];
    if (Record == null) return null;

    const record = new Record(wevent.data);
    return record;
  }

  addHandler(handler) {
    this.handlers[handler.type()] = handler;
    this.writeous.on(handler.type(), this.addRecord.bind(this));
  }

  addRecord(wevent) {
    if (!this.recording) return;
    
    const record = this.weventToRecord(wevent);
    if (record == null) return;

    const lastRecord = this.records.slice(-1)[0];
    if (lastRecord != null && lastRecord.canMerge(record)) {
      lastRecord.merge(record);
      return;
    }

    this.records.push(record);
    if (this.overwriteRedos) this.redos = [];
  }

  undo() {
    const record = this.records.pop();
    if (record == null) return;

    this.doNotRecord(() => record.undo(this.writeous));

    this.redos.push(record);
  }

  redo() {
    const record = this.redos.pop();
    if (record == null) return;
    
    this.preserveRedos(() => record.redo(this.writeous));
  }

  doNotRecord(f) {
    this.recording = false;
    f();
    this.recording = true;
  }

  preserveRedos(f) {
    this.overwriteRedos = false;
    f();
    this.overwriteRedos = true;
  }

}
