
class Zone {
  constructor(size, tags = []) {
    this.size = size;
    this.tags = new Set(tags);
    this.p = null;
    this.n = null;
  }

  getTags() {
    return [...this.tags];
  }

  setTags(tags) {
    this.tags = new Set(tags);
  }

  addTags(tags) {
    this.tags = new Set([...this.tags, ...tags]);
  }

  hasTag(tag) {
    return this.tags.has(tag);
  }

  hasAllTags(tags) {
    return tags.every((tag) => (this.hasTag(tag)));
  }

  hasAnyTags(tags) {
    return tags.some((tag) => (this.hasTag(tag)));
  }

  removeTags(tags) {
    tags.forEach((tag) => {
      this.tags.delete(tag);
    });
  }

  splitAfter(index, tags) {
  }

  removeSelf() {
    if (this.n) this.n.p = this.p;
    if (this.p) this.p.n = this.n;
    this.n = null;
    this.p = null;
    return this.getTags();
  }

  append(size, tags) {
    if (size === 0) return this;
    const zone = new Zone(size, tags);
    return this.appendZone(zone);
  }

  appendZone(zone) {
    this.n = zone;
    // eslint-disable-next-line no-param-reassign
    zone.p = this;
    return zone;
  }

  // FIXME: is this for internal use only? should it be hidden?
  insertZoneAfter(zone) {
    // eslint-disable-next-line no-param-reassign
    zone.p = this;

    // eslint-disable-next-line no-param-reassign
    zone.n = this.n;

    if(this.n != null) this.n.p = zone;
    this.n = zone;
    return zone;
  }

  insertAfter(size, tags) {
    if (size === 0) return null;
    const zone = new Zone(size, tags);
    return this.insertZoneAfter(zone);
  }

  getIndex() {
    if(this.p == null) return 0;

    return this.p.getIndex() + this.p.size;
  }

  compare(size) {
    // eslint-disable-next-line no-use-before-define
    return Zone3.compare(this.size, size);
  }
}

export default class Zone3 {
  constructor(size, tags = []) {
    this.head = new Zone(size, tags);
  }

  getData() {
    let data = [];
    let node = this.head;
    while (node != null) {
      data.push({ size: node.size, tags: node.getTags() });
      node = node.n;
    }

    return data;
  }

  setData(data) {
    this.head = new Zone(0);
    if (data == null) return;

    let node = this.head;
    data.forEach((datum) => {
      node = node.append(datum.size, datum.tags);
    });
  }

  zoneAt(index, excludeStart = true) {
    let offset = 0;
    if (Zone3.compare(index, offset) < 0) throw new Error('index out of range');

    const comparator = (excludeStart) ? 0 : 1;
    let zone = this.head;
    while (zone && Zone3.compare(zone.size + offset, index) < comparator) {
      offset = Zone3.sum(zone.size, offset);
      zone = zone.n;
    }

    //if (!zone) throw new Error('index out of range');
    
    return zone;
  }

  tagsAt(index) {
    const zone = this.zoneAt(index);
    return (zone != null) ? zone.getTags() : [];
  }

  append(size, value) {
    this.tail().append(size, value);
  }

  tail() {
    let zone = this.head;
    while(zone.n) zone = zone.n;
    return zone;
  }

  replace(index, size, tags) {
    if(size === 0) return this;

    const replacement = new Zone(size, tags);
    const start = this.zoneAt(index);
    const newStartSize = (index - start.getIndex());
    const newEndIndex = index + size;
    const end = this.zoneAt(newEndIndex);

    if(end != null) {
      const newEndSize = end.size - (newEndIndex - end.getIndex());
      end.size = newEndSize;
      replacement.appendZone(end);
    }

    start.appendZone(replacement);
    start.size = newStartSize;
    return this;
  }

  insert(index, size, tags, inherit = false, exceptions = []) {
    const oldZone = this.zoneAt(index);
    let newTags = tags;
    if (inherit) {
      newTags = [...oldZone.getTags(), ...tags].filter(tag => !exceptions.includes(tag));
    }

    const firstSize = (index - oldZone.getIndex());
    const secondSize = ((oldZone.getIndex() + oldZone.size) - index);
    oldZone.size = firstSize;
    oldZone.insertAfter(size, newTags).insertAfter(secondSize, oldZone.getTags());
    return this;
  }

  embed(index, size, tags, exceptions) {
    this.insert(index, size, tags, true, exceptions);
    return this;
  }

  static compare(a, b) {
    const diff = a - b;
    if (diff === 0) return 0;

    if (diff > 0) return 1;

    return -1;
  }

  // FIXME: this leaves size-0 nodes. Remove them in the simple-node refactor
  remove(index, size) {
    let remaining = size;

    let node = this.zoneAt(index, false);
    if (node == null) return;

    let offsetSize = node.size - (index - node.getIndex());
    while (node && remaining > 0) {
      if (node.size >= remaining) {
        node.size -= remaining;
        return this;
      } else {
        node.size -= offsetSize;
        remaining -= offsetSize;
        node = node.n;
        offsetSize = node.size;
      }
    }
    return this;
  }

  apply(index, size, tags, unapply = false) {
    if (size === 0) return this;

    const start = this.zoneAt(index, false);
    const firstStartSize = (index - start.getIndex());
    const secondStartSize = start.size - firstStartSize;
    let applyNode = start;
    if(firstStartSize > 0) {
      start.size = firstStartSize;
      applyNode = start.insertAfter(secondStartSize, start.getTags());
      if (applyNode == null) applyNode = start.n;
    }
    const newEndIndex = index + size;
    const end = this.zoneAt(newEndIndex, false);
    let applyEnd = end;
    if (end != null) {
      const secondEndSize = end.size - (newEndIndex - end.getIndex());
      const firstEndSize = end.size - secondEndSize;
      if (firstEndSize > 0) {
        end.size = firstEndSize;
        applyEnd = end.insertAfter(secondEndSize, end.getTags());
      }
    }

    while (applyNode !== applyEnd) {
      if (unapply) {
        applyNode.removeTags(tags);
      } else {
        applyNode.addTags(tags);
      }
      applyNode = applyNode.n;
    }

    return this;
  }

  unapply(index, size, tags) {
    return this.apply(index, size, tags, true);
  }

  applyMatchingAll(matchTags, applyTags) {
    this.zonesWithAll(matchTags).forEach((zone) => {
      zone.addTags(applyTags);
    });
    return this;
  }

  applyMatchingAny(matchTags, applyTags) {
    this.zonesWithAny(matchTags).forEach((zone) => {
      zone.addTags(applyTags);
    });
    return this;
  }

  unapplyMatchingAll(matchTags, applyTags) {
    this.zonesWithAll(matchTags).forEach((zone) => {
      zone.removeTags(applyTags);
    });
    return this;
  }

  unapplyMatchingAny(matchTags, applyTags) {
    this.zonesWithAny(matchTags).forEach((zone) => {
      zone.removeTags(applyTags);
    });
    return this;
  }

  allZones() {
    const zones = [];
    let node = this.head;
    while (node != null) {
      zones.push(node);
      node = node.n;
    }
    return zones;
  }

  zonesWithAll(tags) {
    const zones = [];
    let node = this.head;
    while (node != null) {
      if (node.hasAllTags(tags)) zones.push(node);
      node = node.n;
    }
    return zones;
  }

  zonesWithAny(tags) {
    const zones = [];
    let node = this.head;
    while (node != null) {
      if (node.hasAnyTags(tags)) zones.push(node);
      node = node.n;
    }
    return zones;
  }

  formatTagsByIndex(start, end, customFormatter) {
    const formatter = customFormatter || function defaultFormatter(tags) {
      return tags;
    }

    if (start === end) return {0: formatter([])};

    const startZone = this.zoneAt(start);
    const tags = { 0: formatter(startZone.getTags()) };
    const range = end - start;
    let zone = startZone.n;
    let index = (startZone.getIndex() + startZone.size) - start;
    while (zone && index < range) {
      tags[index] = formatter(zone.getTags());
      index += zone.size;
      zone = zone.n;
    }

    return tags;
  }

  static sum(a, b) {
    return a + b;
  }

  static get Zone() { return Zone; }
}

